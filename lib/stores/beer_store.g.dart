// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'beer_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$BeerStore on _BeerStore, Store {
  Computed<bool> _$hasBeersComputed;

  @override
  bool get hasBeers =>
      (_$hasBeersComputed ??= Computed<bool>(() => super.hasBeers)).value;

  final _$beersAtom = Atom(name: '_BeerStore.beers');

  @override
  ObservableList<Beer> get beers {
    _$beersAtom.context.enforceReadPolicy(_$beersAtom);
    _$beersAtom.reportObserved();
    return super.beers;
  }

  @override
  set beers(ObservableList<Beer> value) {
    _$beersAtom.context.conditionallyRunInAction(() {
      super.beers = value;
      _$beersAtom.reportChanged();
    }, _$beersAtom, name: '${_$beersAtom.name}_set');
  }

  final _$isLoadingAtom = Atom(name: '_BeerStore.isLoading');

  @override
  bool get isLoading {
    _$isLoadingAtom.context.enforceReadPolicy(_$isLoadingAtom);
    _$isLoadingAtom.reportObserved();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.context.conditionallyRunInAction(() {
      super.isLoading = value;
      _$isLoadingAtom.reportChanged();
    }, _$isLoadingAtom, name: '${_$isLoadingAtom.name}_set');
  }

  final _$fetchBeersAsyncAction = AsyncAction('fetchBeers');

  @override
  Future<void> fetchBeers() {
    return _$fetchBeersAsyncAction.run(() => super.fetchBeers());
  }
}
