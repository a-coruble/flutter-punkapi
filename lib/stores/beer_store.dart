import 'package:flutter_mobx_punkapi/apis/punk_api.dart';
import 'package:mobx/mobx.dart';

import 'package:flutter_mobx_punkapi/models/models.dart';

part 'beer_store.g.dart';

class BeerStore = _BeerStore with _$BeerStore;

abstract class _BeerStore with Store {
  final PunkApiClient client = PunkApiClient();
  int page = 1;
  bool hasReachedMax = false;

  @observable
  ObservableList<Beer> beers = ObservableList.of([]);

  @observable
  bool isLoading = false;

  @computed
  bool get hasBeers => beers.length > 0;

  @action
  Future<void> fetchBeers() async {
    if (!hasReachedMax) {
      final List<Beer> newBeers = await client.fetchBeers(page);
      if (newBeers.length == 0) {
        hasReachedMax = true;
      } else {
        beers.addAll(newBeers);
        page += 1;
      }
    }
  }
}
