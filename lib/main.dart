import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_mobx_punkapi/stores/beer_store.dart';
import 'package:flutter_mobx_punkapi/widgets/molecules/beer_card.dart';
import 'package:flutter_mobx_punkapi/widgets/organisms/beer_grid.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final BeerStore _beerStore = BeerStore();
  ScrollController _scrollController;

  _scrollListener() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      _beerStore.fetchBeers();
    }
  }

  @override
  void initState() {
    super.initState();
    _beerStore.fetchBeers();
    _scrollController = new ScrollController();
    _scrollController.addListener(_scrollListener);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Observer(
        builder: (_) {
          if (!_beerStore.hasBeers) {
            if (_beerStore.isLoading) {
              return Container(
                child: CircularProgressIndicator(),
                alignment: Alignment.center,
              );
            }
            return Container();
          } else if (_beerStore.hasBeers) {
            return Container(
                child: Stack(
              children: <Widget>[
                // BeerGrid(
                //   beers: _beerStore.beers,
                //   loadMore: _beerStore.fetchBeers,
                // ),
                ListView(
                  children: _beerStore.beers
                      .map((beer) => BeerCard(
                            beer: beer,
                          ))
                      .toList(),
                  controller: _scrollController,
                ),
                _beerStore.isLoading
                    ? CircularProgressIndicator()
                    : Container(),
              ],
            ));
          }
          return Container();
        },
      ),
    );
  }
}
