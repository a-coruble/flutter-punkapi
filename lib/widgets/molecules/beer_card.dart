import 'package:flutter/material.dart';

import 'package:flutter_mobx_punkapi/models/models.dart';

class BeerCard extends StatelessWidget {
  final Beer beer;

  BeerCard({this.beer});

  @override
  Widget build(BuildContext context) {
    // return Container(
    //   height: 400,
    //   child: Column(
    //     crossAxisAlignment: CrossAxisAlignment.center,
    //     mainAxisSize: MainAxisSize.min,
    //     children: <Widget>[
    //       Card(
    //         elevation: 18.0,
    //         shape: RoundedRectangleBorder(
    //             borderRadius: BorderRadius.all(Radius.circular(10.0))),
    //         child: Image.network(
    //           beer.imageUrl,
    //           fit: BoxFit.contain,
    //         ),
    //         clipBehavior: Clip.antiAlias,
    //         margin: EdgeInsets.all(8.0),
    //       ),
    //       Text(
    //         beer.name,
    //         style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
    //       )
    //     ],
    //   ),
    // );
    return Card(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            new Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(8.0),
              child: Image.network(beer.imageUrl),
              height: 200,
            ),
            new Container(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(beer.name, style: Theme.of(context).textTheme.title),
                  Text(beer.tagline,
                      style: TextStyle(color: Colors.black.withOpacity(0.5))),
                  Text(beer.description.length > 120
                      ? beer.description.substring(0, 120)
                      : beer.description),
                ],
              ),
            )
          ],
          crossAxisAlignment: CrossAxisAlignment.start,
        ));
  }
}
