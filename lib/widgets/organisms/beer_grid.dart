import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import 'package:flutter_mobx_punkapi/models/models.dart';
import 'package:flutter_mobx_punkapi/widgets/molecules/beer_card.dart';

class BeerGrid extends StatefulWidget {
  final List<Beer> beers;
  final Function loadMore;

  BeerGrid({@required this.beers, @required this.loadMore});

  @override
  _BeerGridState createState() =>
      _BeerGridState(beers: beers, loadMore: loadMore);
}

class _BeerGridState extends State<BeerGrid> {
  final List<Beer> beers;
  ScrollController _scrollController;
  final Function loadMore;

  _BeerGridState({@required this.beers, @required this.loadMore});

  _scrollListener() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      loadMore();
    }
  }

  @override
  void initState() {
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StaggeredGridView.count(
      controller: _scrollController,
      crossAxisCount: 4,
      children: beers.map((beer) => BeerCard(beer: beer)).toList(),
      staggeredTiles:
          beers.map<StaggeredTile>((_) => StaggeredTile.fit(2)).toList(),
      primary: false,
    );
  }
}
