import 'package:flutter/material.dart';

import 'package:flutter_mobx_punkapi/models/models.dart';

class BeerPicture extends StatelessWidget {
  final Beer beer;

  BeerPicture({this.beer});

  @override
  Widget build(BuildContext context) {
    return Image.network(
      beer.imageUrl,
    );
  }
}
