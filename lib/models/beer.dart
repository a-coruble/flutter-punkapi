import 'package:equatable/equatable.dart';

class Beer extends Equatable {
  final int id;
  final String name;
  final String tagline;
  final String firstBrewed;
  final String description;
  final String imageUrl;
  final num abv;
  final num ibu;
  final num targetFg;
  final num targetOg;
  final num ebc;
  final num srm;
  final num ph;
  final num attenuationLevel;
  final List<String> foodPairing;
  final String brewersTips;
  final String contributedBy;

  Beer(
      {this.id,
      this.name,
      this.tagline,
      this.firstBrewed,
      this.description,
      this.imageUrl,
      this.abv,
      this.ibu,
      this.targetFg,
      this.targetOg,
      this.ebc,
      this.srm,
      this.ph,
      this.attenuationLevel,
      this.foodPairing,
      this.brewersTips,
      this.contributedBy});

  Beer.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        tagline = json['tagline'],
        firstBrewed = json['first_brewed'],
        description = json['description'],
        imageUrl = json['image_url'],
        abv = json['abv'],
        ibu = json['ibu'],
        targetFg = json['target_fg'],
        targetOg = json['target_og'],
        ebc = json['ebc'],
        srm = json['srm'],
        ph = json['ph'],
        attenuationLevel = json['attenuation_level'],
        foodPairing = json['food_pairing'].cast<String>(),
        brewersTips = json['brewers_tips'],
        contributedBy = json['contributed_by'];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['tagline'] = this.tagline;
    data['first_brewed'] = this.firstBrewed;
    data['description'] = this.description;
    data['image_url'] = this.imageUrl;
    data['abv'] = this.abv;
    data['ibu'] = this.ibu;
    data['target_fg'] = this.targetFg;
    data['target_og'] = this.targetOg;
    data['ebc'] = this.ebc;
    data['srm'] = this.srm;
    data['ph'] = this.ph;
    data['attenuation_level'] = this.attenuationLevel;
    data['food_pairing'] = this.foodPairing;
    data['brewers_tips'] = this.brewersTips;
    data['contributed_by'] = this.contributedBy;
    return data;
  }

  @override
  List<Object> get props => [
        id,
        name,
        tagline,
        firstBrewed,
        description,
        imageUrl,
        abv,
        ibu,
        targetFg,
        targetOg,
        ebc,
        srm,
        ph,
        attenuationLevel,
        foodPairing,
        brewersTips,
        contributedBy
      ];

  @override
  String toString() => """Beer { 
        id: $id, 
        name: $name, 
        tagline: $tagline, 
        firstBrewed: $firstBrewed, 
        description: $description, 
        imageUrl: $imageUrl, 
        abv: $abv, 
        ibu: $ibu, 
        targetFg: $targetFg,
        targetOg: $targetOg, 
        ebc: $ebc,
        srm: $srm,
        ph: $ph,
        attenuationLevel: $attenuationLevel,
        foodPairing: $foodPairing,
        brewersTips: $brewersTips,
        contributedBy: $contributedBy
        }""";
}
